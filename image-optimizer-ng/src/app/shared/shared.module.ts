import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ErrorsComponent } from './components/errors/errors.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ErrorsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FontAwesomeModule,
  ],
  exports: [
    CommonModule,
    HeaderComponent,
    FooterComponent,
    ErrorsComponent,
    FormsModule,
    RouterModule,
    FontAwesomeModule,
  ]
})
export class SharedModule { }
