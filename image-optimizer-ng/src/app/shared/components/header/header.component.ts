import { Component, HostListener, OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/core/services/user.service';
import { faUser, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: any;
  isAuth: boolean = false;
  faUser = faUser;
  faChevronDown = faChevronDown;
  showAccMenu: boolean = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userService.currentUser.subscribe(data => this.user = data);
    this.userService.isAuthenticated.subscribe(data => this.isAuth = data);
  }

  @HostListener('document:click', ['$event'])
  documentClick(event: MouseEvent) {
    let className = (event.target as Element).className;
    if(this.showAccMenu && typeof className == 'string' && className.indexOf('Header__AccountToggle') == -1) this.showAccMenu = false;
  }

  logOut(){
    this.userService.purgeAuth();
    this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
  }
  
}
