import { Component } from '@angular/core';
import { UserService } from './modules/core/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private userService: UserService
  ) { 
    this.userService.refreshUserData();
  }
}
