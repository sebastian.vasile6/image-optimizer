import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { RouterModule } from '@angular/router';
import { AccountRoutingModule } from './account-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { ApiKeysComponent } from './api-keys/api-keys.component';
import { ApiUsageComponent } from './api-usage/api-usage.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    DashboardComponent,
    AccountComponent,
    ProfileComponent,
    ApiKeysComponent,
    ApiUsageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AccountRoutingModule,
    SharedModule
  ]
})
export class AccountModule { }
