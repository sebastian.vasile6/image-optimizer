import { Component, OnInit } from '@angular/core';
import { ApiKey } from '../../core/models/apikey';
import { User } from '../../core/models/user';
import { ApiService } from '../../core/services/api.service';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-api-keys',
  templateUrl: './api-keys.component.html',
  styleUrls: ['./api-keys.component.scss']
})
export class ApiKeysComponent implements OnInit {
  user: User;
  apiKeys: Array<ApiKey> = [];
  errors: Array<string> = [];
  isCreating: boolean = false;

  constructor(
    private apiService: ApiService,
    private userService: UserService
  ) { 
    this.user = this.userService.getCurrentUser();
  }

  ngOnInit(): void {
    this.apiService.get('/apikeys').subscribe(
      data => {
        this.apiKeys = data;
      },
      err => {
        this.errors.push('Error retrieving API Keys. Please try again later!')
      }
    )
  }

  createKey(){
    this.errors = [];
    this.isCreating = true;

    this.apiService.post('/apikeys', {userid: this.user.id}).subscribe(
      data => {
        this.apiKeys.push(data);
        this.isCreating = false;
      },
      err => {
        if(typeof err == 'string'){
          this.errors.push(err)
        }else{
          this.errors.push('Error generating the API Key. Please try again later!')
        }
        this.isCreating = false;
      }
    )
  }

}
