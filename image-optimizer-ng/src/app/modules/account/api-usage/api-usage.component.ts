import { Component, OnInit } from '@angular/core';
import { ApiUsage } from '../../core/models/apiusage';
import { User } from '../../core/models/user';
import { ApiService } from '../../core/services/api.service';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'app-api-usage',
  templateUrl: './api-usage.component.html',
  styleUrls: ['./api-usage.component.scss']
})
export class ApiUsageComponent implements OnInit {
  user: User;
  apiUsage: Array<ApiUsage> = [];
  apiUsageShow: Array<ApiUsage> = [];
  errors: Array<string> = [];
  currentPage: number = 1;
  pageSize: number = 20;
  pages: Array<number> = [];

  constructor(
    private apiService: ApiService,
    private userService: UserService
  ) {
    this.user = this.userService.getCurrentUser();
  }

  ngOnInit(): void {
    this.apiService.get('/apiusage').subscribe(
      data => {
        this.apiUsage = data;
        this.apiUsageShow = this.apiUsage.slice(0, this.pageSize);
        for(let i = 1; i <= Math.ceil(this.apiUsage.length/this.pageSize); i++){
          this.pages.push(i);
        }
      },
      err => {
        this.errors.push('Error retrieving API Usage. Please try again later!')
      }
    )
  }

  changePage(page: number){
    this.currentPage = page;
    this.apiUsageShow = this.apiUsage.slice((this.currentPage - 1) * this.pageSize, this.currentPage * this.pageSize);
  }

}
