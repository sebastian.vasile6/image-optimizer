import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';

import { ApiService } from './api.service';
import { User } from '../models/user';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(0);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();
  
  constructor (
    private apiService: ApiService,
    private router: Router
  ) {}

  refreshUserData() {
    let storageUser = this.getStorageUser();
    if (storageUser) {
      this.apiService.get('/user/' + storageUser.id)
      .subscribe(
        data => this.setAuth(data),
        err => {
          this.purgeAuth();
          this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
        }
      );
    } else {
      this.purgeAuth();
    }
  }

  setAuth(user: User) {
    this.setStorageUser(user);
    this.currentUserSubject.next(user);
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    this.removeStorageUser();
    this.currentUserSubject.next({} as User);
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(type: string, credentials: any): Observable<User> {
    let route = (type === 'login') ? '/login' : '/signup';
    return this.apiService.post(route, credentials)
      .pipe(map(
      data => {
        this.setAuth(data);
        return data;
      }
    ));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  private setStorageUser(user: User){
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  getStorageUser(){
    let user,
    storageVal = localStorage.getItem('currentUser');
    if(storageVal)
      user = <User>JSON.parse(storageVal);
    return user;
  }

  private removeStorageUser(){
    localStorage.removeItem('currentUser');
  }

}