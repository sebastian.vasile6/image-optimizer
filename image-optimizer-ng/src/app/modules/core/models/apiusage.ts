export interface ApiUsage {
  id: number;
  date: string;
  api_key: string;
  origin: string;
  user_agent: string;
}