export interface ApiKey {
  id: number;
  created: string;
  user: string;
  label: string;
  api_key: string;
  status: string;
}