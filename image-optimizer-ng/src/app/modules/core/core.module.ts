import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthGuard } from './services/auth.guard';
import { ApiService } from './services/api.service';
import { UserService } from './services/user.service';
import { HttpTokenInterceptor } from './interceptors/http.token.interceptor';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    AuthGuard,
    ApiService,
    UserService
  ]
})
export class CoreModule { }
