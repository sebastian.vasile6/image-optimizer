import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  title: string = '';
  authType: string = '';
  errors: Array<string> = new Array<string>();
  isSubmitting: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { 
    this.authForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'password': new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
    this.route.url.subscribe(data => {
      this.authType = data[data.length - 1].path;
      this.title = (this.authType === 'login') ? 'Log in' : 'Sign up';
      if (this.authType === 'signup') {
        this.authForm.addControl('email', new FormControl(null, [Validators.required, Validators.email]));
      }
    });

    this.userService.isAuthenticated.subscribe(data => {
      if(data == true){
        let returnUrl = this.route.snapshot.queryParams.returnUrl;
        console.log(this.route.snapshot.queryParams);
        if(!returnUrl) returnUrl = '/account/dashboard';
        console.log(returnUrl);
        this.router.navigateByUrl(returnUrl);
      }
    });
  }

  auth(){
    if(this.isSubmitting) return;

    this.errors = [];

    if(!this.authForm.valid){
      if(!this.authForm.controls['username'].valid){
        this.errors.push('Username is mandatory');
      }
      if(this.authForm.controls['email'] && !this.authForm.controls['email'].valid){
        if(this.authForm.controls['email'].value){
          this.errors.push('Email is invalid');
        }else{
          this.errors.push('Email is mandatory');
        }
      }
      if(!this.authForm.controls['password'].valid){
        this.errors.push('Password is mandatory');
      }
      return;
    }

    this.isSubmitting = true;

    this.userService.attemptAuth(this.authType, this.authForm.value).subscribe(
      data => {
        //this.router.navigateByUrl('/account')
        this.isSubmitting = false;
      },
      err => {
        if(typeof err == 'string'){
          this.errors.push(err);
        }else{
          this.errors.push('ERROR! Please try again later!');
        }
        this.isSubmitting = false;
      }
    );
  }
}
