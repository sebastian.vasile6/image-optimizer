import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/modules/core/services/api.service';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  contactForm: FormGroup;
  errors: Array<string> = new Array<string>();
  isSubmitting: boolean = false;
  isSent: boolean = false;

  constructor(
    private apiService: ApiService
  ) { 
    this.contactForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'message': new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
  }

  contact(){
    if(this.isSubmitting || this.isSent) return;

    this.errors = [];

    if(!this.contactForm.valid){
      if(!this.contactForm.controls['name'].valid){
        this.errors.push('Name is mandatory');
      }
      if(!this.contactForm.controls['email'].valid){
        if(this.contactForm.controls['email'].value){
          this.errors.push('Email is invalid');
        }else{
          this.errors.push('Email is mandatory');
        }
      }
      if(!this.contactForm.controls['message'].valid){
        this.errors.push('Message is mandatory');
      }
      return;
    }

    this.isSubmitting = true;

    this.apiService.post('/contact', this.contactForm.value).subscribe(
      data => {
        this.isSent = true;
        this.isSubmitting = false;
      },
      err => {
        this.errors.push('ERROR! Please try again later!');
        this.isSubmitting = false;
      }
    )
  }

}
