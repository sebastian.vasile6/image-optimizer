'use strict';

var express = require('express');
var app = express();

const cors = require('cors');
app.use(cors({
    origin: 'http://localhost:4200'
}));

app.set("port", process.env.PORT || 4000);

app.get('/', function (req, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  var response = { "response" : "This is GET method." }
  console.log(response);
  res.end(JSON.stringify(response));
})

app.get('/apikeys', function (req, res) {
  var response = [{ 
    id: 1,
    created: 'Jul 15, 2021 7:02 am',
    user: 'username',
    label: 'label1',
    api_key: '2d07aa5e145f48b2defc99c2ebe47012',
    status: 'Active',
  },{ 
    id: 2,
    created: 'Jul 15, 2021 7:02 am',
    user: 'username',
    label: 'label2',
    api_key: '2d07aa5e145f48b2defc99c2ebe47012',
    status: 'Active',
  }]
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(response));
})

app.post('/apikeys', function (req, res) {
  var response = { 
    id: 3,
    created: 'Jul 15, 2021 7:02 am',
    user: 'username',
    label: 'label3',
    api_key: '2d07aa5e145f48b2defc99c2ebe47012',
    status: 'Active',
  }
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(response));
})

app.get('/apiusage', function (req, res) {
  var response = [];
  for(var i = 0; i < 102; i++){
    response.push({ 
      id: 2,
      date: 'Jul 15, 2021 7:02 am',
      origin: 'http://localhost:4200/',
      user_agent: 'Mozilla ' + i,
      api_key: '2d07aa5e145f48b2defc99c2ebe47012',
    });
  }
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(response));
})

app.post('/login', function (req, res) {
  var response = { 
    id: 1,
    username: 'username',
    email: 'test@email.com',
    token: 'test_token',
  }
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(response));
})

app.post('/contact', function (req, res) {
  var response = { 
    message: 'message sent'
  }
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(response));
})

app.post('/signup', function (req, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  
  var response = { 
    id: 1,
    username: 'username',
    email: 'test@email.com',
    token: 'test_token',
  }
  res.end(JSON.stringify(response));
})

app.get('/user/:id', function (req, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  
  var response = { 
    id: 1,
    username: 'username',
    email: 'test@email.com',
    token: 'test_token',
  }
  res.end(JSON.stringify(response));
})

var server = app.listen(app.get("port"), function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Node.js API app listening at http://%s:%s", host, port)

})